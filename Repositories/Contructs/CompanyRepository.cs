﻿using Database;
using Microsoft.EntityFrameworkCore;
using Models.Entity;
using Repositories.Base;
using RepositoryAbstructions.Base;
using RepositoryAbstructions.Contructs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.Contructs
{
    public class CompanyRepository : Repository<Company>, ICompanyRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public CompanyRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<Company>> GetAllCompany()
        {
    

            var data =await _dbContext.Companies
                .Include(c => c.PropertyValues)
                .Include(c => c.Client)
                .ThenInclude(c=>c.ExtraCompanyProperties)
                .ToListAsync();



            return data;
        }

        public override async Task<Company> GetByIdAsync(long id)
        {
            return await _dbContext.Companies.Where(c => c.Id == id)
              .Include(c => c.PropertyValues)
              .Include(c => c.Client)
              .ThenInclude(c => c.ExtraCompanyProperties)
              .FirstOrDefaultAsync(); 
        }

        public async Task<List<Company>> GetCompnaiesForCLient(long clientId)
        {
            var data = await _dbContext.Companies.Where(c=>c.ClientId==clientId)
              .Include(c=>c.PropertyValues)
              .Include(c => c.Client)
              .ThenInclude(c => c.ExtraCompanyProperties)
              .ToListAsync();



            return data;
        }
    }
}
