﻿using Database;
using Models.Entity;
using Repositories.Base;
using RepositoryAbstructions.Contructs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.Contructs
{
    public class ExtraCompanyPropertyRepository : Repository<ExtraCompanyProperty>, IExtraCompanyPropertyRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public ExtraCompanyPropertyRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }
    }
}
