﻿using AutoMapper;
using ManagerAbstruction.Contructs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models.CreateDto;
using Models.Entity;
using Models.ReturnDto;

namespace CompanyManagement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExtraCompanyPropertyController : ControllerBase
    {
        private readonly IExtraCompanyPropertyManager _propertyService;
        private readonly IMapper _mapper;

        public ExtraCompanyPropertyController(IExtraCompanyPropertyManager propertyService, IMapper mapper)
        {
            _propertyService = propertyService;
            _mapper = mapper;
        }


        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ExtraCompanyPropertyCreateDto model)
        {
            try
            {
                if (model is null) return BadRequest("Invalid input");

                var addableProduct = _mapper.Map<ExtraCompanyProperty>(model);

                var result = await _propertyService.AddAsync(addableProduct);

                if (result is true)
                {
                    return Ok(addableProduct);
                }

                return BadRequest("Failed to create bank");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       


    }
}