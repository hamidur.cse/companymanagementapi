﻿using AutoMapper;
using ManagerAbstruction.Contructs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models.CreateDto;
using Models.Entity;
using Models.ReturnDto;

namespace CompanyManagement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyController : ControllerBase
    {
        private readonly ICompanyManager _companyService;
        private readonly IMapper _mapper;

        public CompanyController(ICompanyManager companyService, IMapper mapper)
        {
            _companyService = companyService;
            _mapper = mapper;
        }


        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CompanyCreateDto model)
        {
            try
            {
                if (model is null) return BadRequest("Invalid input");

                var addableProduct = _mapper.Map<Company>(model);

                var result = await _companyService.AddAsync(addableProduct);

                if (result is true)
                {
                    return Ok(new { message = "Created Successfully" });
                }

                return BadRequest("Failed to create bank");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var companies = await _companyService.GetAllCompany();
                var data = _mapper.Map<IList<CompanyReturnDto>>(companies);
                if (data is not null)
                {
                    return Ok(data);
                }

                return NotFound();
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [HttpGet("GetCompnaiesForCLient")]
        public async Task<IActionResult> GetCompnaiesForCLient(long clientId)
        {
            try
            {
                var companies = await _companyService.GetCompnaiesForCLient(clientId);
                var data = _mapper.Map<IList<CompanyReturnDto>>(companies);
                if (data is not null)
                {
                    return Ok(data);
                }

                return NotFound();
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(long id, [FromBody] CompanyCreateDto model)
        {
            try
            {
                if (model is null || id <= 0) return BadRequest("Invalid input");

                bool result = false;
                var existingItem = await _companyService.GetFirstOrDefaultAsync(c => c.Id == id);

                var updateAbleItem = _mapper.Map(model, existingItem);



                result = await _companyService.UpdateAsync(updateAbleItem);

                if (result is true)
                {

                    return Ok(new {message="Updated Successfully"});
                }

                return BadRequest("Failed to update");
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(long id)
        {
            if (id <= 0) return BadRequest("Invalid Id");

            var item = await _companyService.GetByIdAsync(id);

            var returnItem = _mapper.Map<CompanyReturnDto>(item);

            if (returnItem is null)
                return NotFound();

            return Ok(returnItem);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            try
            {
                if (id <= 0) return BadRequest("Invalid input");

                var deleteableItem = await _companyService.GetByIdAsync(id);

                if (deleteableItem is null) return NotFound();

                var result = await _companyService.RemoveAsync(deleteableItem);

                if (result is true) return Ok(result);

                return BadRequest("Failed to delete bank");
            }
            catch (Exception ex)
            {
                throw;
            }
        }

       
    }
}
