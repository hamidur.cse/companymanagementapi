﻿using Database;
using Manager.Contracts;
using ManagerAbstruction.Contructs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Repositories.Contructs;
using RepositoryAbstructions.Contructs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Configuration
{
    public class ServiceConfigurations
    {
        public static void Configuration(IServiceCollection services, IConfiguration configuration)
        {

            services.AddDbContext<ApplicationDbContext>(opts =>
                opts.UseSqlServer(configuration.GetConnectionString("AppConnectionString"),
                x => x.MigrationsAssembly("Database")
            ));


            //Company
            services.AddTransient<ICompanyManager, CompanyManager>();
            services.AddTransient<ICompanyRepository, CompanyRepository>();

            services.AddTransient<IExtraCompanyPropertyManager, ExtraCompanyPropertyManager>();
            services.AddTransient<IExtraCompanyPropertyRepository, ExtraCompanyPropertyRepository>();
            





        }

    }
}