﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Entity
{
    public class PropertyValue
    {
        public long Id { get; set; }
        public string Value { get; set; }
        public long ExtraCompanyPropertyId { get; set; }
        public ExtraCompanyProperty ExtraCompanyProperty { get; set; }
        public long CompanyId { get; set; }
        public Company Company { get; set; }

    }
}
