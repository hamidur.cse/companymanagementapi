﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Entity
{
    public class ExtraCompanyProperty
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long ClientId { get; set; }
        public Client Client { get; set; }
    }
}
