﻿using Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ReturnDto
{
    public class ClientReturnDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public ICollection<ExtraCompanyPropertyReturnDto>? ExtraCompanyProperties { get; set; }
    }
}
