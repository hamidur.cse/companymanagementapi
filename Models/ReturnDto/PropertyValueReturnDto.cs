﻿using Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ReturnDto
{
    public class PropertyValueReturnDto
    {
        public long Id { get; set; }
        public string Value { get; set; }
        public long ExtraCompanyPropertyId { get; set; }
        public string ExtraCompanyPropertyName { get; set; }
        //public long CompanyId { get; set; }
        //public string CompanyName { get; set; }
    }
}
