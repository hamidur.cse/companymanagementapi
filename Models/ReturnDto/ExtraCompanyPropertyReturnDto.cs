﻿using Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ReturnDto
{
    public class ExtraCompanyPropertyReturnDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long ClientId { get; set; }
    }
}
