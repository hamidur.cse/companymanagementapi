﻿using Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ReturnDto
{
    public class CompanyReturnDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public long? ClientId { get; set; }
        public ClientReturnDto Client { get; set; }
        public ICollection<PropertyValueReturnDto>? PropertyValues { get; set; }
    }
}
