﻿using Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.CreateDto
{
    public class CompanyCreateDto
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public long? ClientId { get; set; }
        public ICollection<PropertyValueCreateDto>? PropertyValues { get; set; }
    }
}
