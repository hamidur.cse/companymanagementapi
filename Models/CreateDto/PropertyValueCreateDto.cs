﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.CreateDto
{
    public class PropertyValueCreateDto
    {
        public long Id { get; set; }
        public string Value { get; set; }
        public long ExtraCompanyPropertyId { get; set; }
        public long CompanyId { get; set; }
    }
}
