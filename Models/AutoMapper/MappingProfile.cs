﻿using AutoMapper;
using Models.CreateDto;
using Models.Entity;
using Models.ReturnDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.AutoMapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {

            //Company
            CreateMap<CompanyCreateDto, Company>();
            CreateMap<Company, CompanyReturnDto>();
            CreateMap<ExtraCompanyPropertyCreateDto, ExtraCompanyProperty>();
            CreateMap<ExtraCompanyProperty, ExtraCompanyPropertyReturnDto>();
            CreateMap<Client, ClientReturnDto>();
            CreateMap<PropertyValueCreateDto, PropertyValue>();
            CreateMap<PropertyValue, PropertyValueReturnDto>();
            




        }
    }
}

