﻿using ManagerAbstruction.Base;
using Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagerAbstruction.Contructs
{
    public interface IExtraCompanyPropertyManager : IManager<ExtraCompanyProperty>
    {

    }
}
