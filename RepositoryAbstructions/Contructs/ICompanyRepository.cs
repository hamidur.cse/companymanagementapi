﻿using Models.Entity;
using RepositoryAbstructions.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositoryAbstructions.Contructs
{
    public interface ICompanyRepository:IRepository<Company>
    {
        Task<List<Company>> GetAllCompany();
        Task<List<Company>> GetCompnaiesForCLient(long clientId);
    }
}
