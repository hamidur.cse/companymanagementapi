﻿using Models.Entity;
using RepositoryAbstructions.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositoryAbstructions.Contructs
{
    public interface IExtraCompanyPropertyRepository : IRepository<ExtraCompanyProperty>
    {
    }
}
