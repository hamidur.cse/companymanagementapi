﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Database.Migrations
{
    /// <inheritdoc />
    public partial class Set_Seed_Data_To_Clients : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
  SET IDENTITY_INSERT [dbo].[Clients] ON 
insert into Clients (Id,Name)
VALUES (1,'Client 1'),(2,'Client 2'),(3,'Client 3')
  SET IDENTITY_INSERT [dbo].[Clients] OFF
"
);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
