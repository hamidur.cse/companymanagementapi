﻿using Manager.Base;
using ManagerAbstruction.Contructs;
using Models.Entity;
using RepositoryAbstructions.Contructs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manager.Contracts
{
    public class ExtraCompanyPropertyManager : Manager<ExtraCompanyProperty>, IExtraCompanyPropertyManager
    {
        private readonly IExtraCompanyPropertyRepository _repository;

        public ExtraCompanyPropertyManager(IExtraCompanyPropertyRepository repository) : base(repository)
        {
            _repository = repository;
        }



    }
}