﻿using Manager.Base;
using ManagerAbstruction.Contructs;
using Models.Entity;
using RepositoryAbstructions.Contructs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manager.Contracts
{
    public class CompanyManager : Manager<Company>, ICompanyManager
    {
        private readonly ICompanyRepository _repository;

        public CompanyManager(ICompanyRepository repository) : base(repository)
        {
            _repository = repository;
        }

        public async Task<List<Company>> GetAllCompany()
        {
            return await _repository.GetAllCompany();
        }

        public async Task<List<Company>> GetCompnaiesForCLient(long clientId)
        {
            return await _repository.GetCompnaiesForCLient(clientId);   
        }



    }
}